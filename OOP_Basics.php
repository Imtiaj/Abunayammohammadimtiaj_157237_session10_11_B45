<?php

class Person{
    public $name;
    //protected $name;
    public $id;
    public $dateOfBirth;

    public function setName($localName)
    {
        $this->name = $localName;
    }

    public function _construct()
    {
     echo "i am mad";
    }

    public function __destruct()
    {
        echo "Alveeda!.<br>";
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }
}

class Student extends Person{
    public function doSomething(){
        echo $this->name."<br>";
    }
}
$objStudent=new student();
$objStudent->setName("Samim");
echo $objStudent->doSomething();

$objPerson = new Person();//name will be different but structure will be same

$objPerson->setName("Sam");

$objMan = new Person();//structure will be same but not name

$objMan->setName("Samir");

echo $objPerson->name."<br>";
unset($objPerson);
echo $objMan->name."<br>";
unset ($objMan);
echo $objStudent->name."<br>";
